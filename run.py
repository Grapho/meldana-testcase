import json

from flask import render_template

from src.source import app
from src.source import my_print

from src.engine import DB_LABELS, get_persons, get_vehicles

from src.general import Messages
from src.general import ProjectBIO
from src.general import MessageTypes


# Вью

@app.route('/', methods=["GET"])
def base_view():
    my_print(Messages.main_page)
    return render_template('base.html')


@app.route('/persons', methods=["GET"])
def persons_view():
    my_print(Messages.persons_page)
    return render_template(
        'persons.html',
        data=get_persons(),
        labels=DB_LABELS
    )


@app.route('/vehicles', methods=["GET"])
def vehicles_view():
    my_print(Messages.vehicles_page)
    return render_template(
        'vehicles.html',
    )


# Функционал

@app.route('/get_vehicles_data', methods=["GET"])
def get_vehicles_data():
    return json.dumps({
        "data": get_vehicles(),
        "labels": DB_LABELS
    })


@app.route('/get_persons_data', methods=["GET"])
def get_persons_data():
    return json.dumps({
        "data": get_persons(),
        "labels": DB_LABELS
    })


my_print(Messages.started, msg_type=MessageTypes.special)
app.run(host=ProjectBIO.host, port=ProjectBIO.port, debug=False)
my_print(Messages.stopped, msg_type=MessageTypes.special)
