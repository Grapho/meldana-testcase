var datatable = new Vue({
    delimiters: ['{*', '*}'],
    el: '.datatable',
    data: {
        data: new Array(),
        labels: new Array(),
        data_const: new Array(),
    }
});


$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "/get_persons_data",
        success: function (response) {
            datatable.data = JSON.parse(response)['data']
            datatable.data_const = JSON.parse(response)['data']
            datatable.labels = JSON.parse(response)['labels']
        },
        error: function () {
            alert("Error in get_persons_data");
        }
    });
})
