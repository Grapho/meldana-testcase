import datetime

from sqlalchemy import create_engine
from sqlalchemy.orm import create_session

from src.source import db

from src.models import Person
from src.models import Vehicle

from src.general import ProjectBIO

DB_LABELS = {
    'person': [
        'ID',
        'ФИО',
        'Возраст',
        'Адрес',
        'Автомобиль',
    ],
    'vehicle': [
        'ID',
        'Модель',
        'Марка',
        'Номер',
    ]
}


class Database(object):
    """
    Класс для управления базой данных.
    """
    def __new__(cls) -> object:
        """
        Класс работает в режиме синглтона.
        Переопределение базовой функции создания экземпляра класса.
        Возвращает экземпляр класса, если класс уже был создан.
        """
        if not hasattr(cls, 'instance'):
            cls.instance = super(Database, cls).__new__(cls)
        engine = create_engine(
            ProjectBIO.db_name,
            echo=False,
            connect_args={'check_same_thread': False}
        )
        db.metadata.create_all(engine)
        db.metadata.bind = engine
        cls.session = create_session(
            bind=engine,
            autocommit=False,
            autoflush=True)

        return cls.instance

    @classmethod
    def get_base(cls, database: object) -> object:
        """
        Возвращает содержимое запрашиваемой базы данных.
        :param database: База данных.
        :return: sqlalchemy.orm.query.Query - запрос к базе данных­
        """
        return cls.session.query(database)

    @classmethod
    def add(cls, base: object) -> object:
        """
        Функция обновление таблицы в базе данных.
        :param base: Принимате на вход таблицу для обновления
        :return: base возвращает принятую таблицу
        """
        cls.session.add(base)
        cls.session.commit()

        return base


database = Database()


def get_persons():
    data = list()
    for d in database.get_base(Person).all():
        data.append({
            'id': d.id,
            'username': d.username,
            'age': d.age,
            'address': d.address,
            'vehicle': d.vehicle.get_data(),
        })
    return data


def get_vehicles():
    data = list()
    for d in database.get_base(Vehicle).all():
        data.append({
            'id': d.id,
            'model': d.model,
            'brand': d.brand,
            'number': d.number,
        })
    return data
