from flask import Flask

from flask_login import LoginManager

from flask_sqlalchemy import SQLAlchemy

from src.general import Writer
from src.general import ProjectBIO


app = Flask(
    __name__,
    template_folder='../templates',
    static_folder='../static',
)
app.config['SECRET_KEY'] = ProjectBIO.secret_key
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

login_manager = LoginManager()

my_print = Writer().my_print
