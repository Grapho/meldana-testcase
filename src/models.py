from src.source import db

from sqlalchemy import String
from sqlalchemy import Integer
from sqlalchemy import Column
from sqlalchemy import ForeignKey

from sqlalchemy.orm import backref


class Vehicle(db.Model):
    """
    Модель базы данных
    """
    __tablename__ = 'vehicle'
    id = Column(Integer, primary_key=True)
    model = Column(String)
    brand = Column(String)
    number = Column(Integer)

    def __init__(self, model, brand, number):
        self.model = model
        self.brand = brand
        self.number = number

    def __str__(self):
        return f"<DB: {self.__tablename__}>, <ID: {self.id}>, <Model: {self.model}>, <Brand: {self.brand}>, <Number: {self.number}>"

    def get_data(self):
        return f"{self.model} {self.brand} ({self.number})"


class Person(db.Model):
    """
    Модель базы данных
    """
    __tablename__ = 'person'
    id = Column(Integer, primary_key=True)
    username = Column(String)
    age = Column(Integer)
    address = Column(String)
    vehicle_id = Column(Integer, ForeignKey('vehicle.id'))
    vehicle = db.relationship(
        Vehicle, backref=backref("vehicle.id"))

    def __init__(self, username, age, address, vehicle):
        self.username = username
        self.age = age
        self.address = address
        self.vehicle = vehicle

    def __str__(self):
        return f"<DB: {self.__tablename__}>, <ID: {self.id}>, <Username: {self.username}>, <Age: {self.age}>, <Address: {self.address}>, <Vehicle: {self.vehicle}>"
