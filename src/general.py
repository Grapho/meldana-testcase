import os
import sys
import logging
import tempfile
import traceback

from termcolor import cprint
from logging.handlers import RotatingFileHandler


EXCEPTION_FORMAT = "{error_type}:\n{error_msg}:\n{traceback_msg}"
LOG_FORMAT = u"%(levelname)-8s - %(name)-22s [LINE:%(lineno)5d] - %(asctime)s - %(message)s"
BASE_FILES_PATH = 'static/files'
BASE_DATE_FORMAT = "%d.%m.%Y"


class ProjectBIO:
    """
    Класс, содержащий основную информцию по проекту.
    """
    port = 2022
    host = "0.0.0.0"
    release_date = "today"  # TODO
    release_version = "1.0.0"
    project_desc = "Тестовый проект"
    project_name = "test_case"
    db_name = 'sqlite:///database.db'
    log_filename = f"{project_name}.log"
    log_dir = f"/var/log/{project_name}/"
    secret_key = "ob2-y818ujq_$s+xqat3vcq!(2xs&-8!g4@_x%7h8&v=@&!9%-"

    def get_project_info(self) -> str:
        """
        Функция возвращает форматированную строку с информацией о проекте.
        """
        return f"Проект: {self.project_name} - {self.project_desc}. Версия: {self.release_version} от {self.release_date}."

    def __str__(self) -> str:
        """
        Функция возвращает форматированную строку с информацией о проекте.
        Перегрузка базовой функции приведения класса к строке.
        """
        return f"Проект: {self.project_name} - {self.project_desc}. Версия: {self.release_version} от {self.release_date}."


class Messages:
    """
    Класс констант сообщений
    """
    started = "Выполнен вход в программу."
    main_page = "Открыта главная страница"
    vehicles_page = "Открыта страница автомобилей"
    empty_data = "Обнаружен пустой файл с данными"
    persons_page = "Открыта страница пользователей"
    cant_create_logger = "Ошибка создания лог-файла."
    create_file = "Файл будет создан в директории: {}"
    cant_create_folder = "Ошибка создания каталога {}"
    catch_error = "Возникла непредвиденная ошибка: [{error}]"
    temp_dir = "Файл будет создан во временной директории: {}"
    no_one_items = "По вашему запросу в БД не найдено ни одного объекта"
    stopped = "Работа приложения остановлена. Выполнен выход из программы."
    separator = '[*][==================[Приложение запущено]==================][*]'
    miss_exception = "Некорректное завершение программы. Необработанное исключение."


class MessageTypes:
    """
    Класс типов сообщений.
    Используется при логгировании.
    """
    info = 'info'
    warning = 'warning'
    critical = 'critical'
    special = 'special'


class Colors:
    """
    Класс атрибутов используемых при выводе на экран командой cprint
    Атрибут цвета текста.
    """
    red = "red"
    blue = "blue"
    grey = "grey"
    cyan = "cyan"
    white = "white"
    green = "green"
    yellow = "yellow"
    magenta = "magenta"


class OnColor:
    """
    Класс атрибутов используемых при выводе на экран командой cprint
    Атрибут выделения текста цветом.
    """
    red = "on_red"
    blue = "on_blue"
    grey = "on_grey"
    cyan = "on_cyan"
    white = "on_white"
    green = "on_green"
    yellow = "on_yellow"
    magenta = "on_magenta"


class Attributes:
    """
    Класс атрибутов используемых при выводе на экран командой cprint
    Атрибут выделения текста разными шрифтами.
    """
    bold = "bold"
    dark = "dark"
    blink = "blink"
    reverse = "reverse"
    underline = "underline"
    concealed = "concealed"


class Logger:
    """
    Класс, обеспечивающий логгирование приложения.
    Содержит все необходимые для работы методы
    """

    def __init__(self):
        """
        Инициализатор класса.
        Создает экземпляр логгера с заданным названием по заданному пути.
        """
        self.log_file = ProjectBIO.log_dir + ProjectBIO.log_filename
        self.logger = self.__create_logger(__name__)
        if not self.logger:
            cprint(Messages.cant_create_logger, color=Colors.red)
            raise Exception(Messages.cant_create_logger)
        sys.excepthook = self.__log_uncaught_exceptions

    def __create_logger(self, name):
        """
        Создание логгера
        :param name: 
        :return: 
        """
        hndlr = self.__get_handler()
        lg = logging.getLogger(name)
        lg.setLevel(logging.INFO)
        lg.addHandler(hndlr)
        lg.info(Messages.separator)
        lg.info(ProjectBIO().get_project_info())
        return lg

    def __get_handler(self):
        """
        Метод для формирования handler для логгирования
        :return: handler
        """
        hndlr = RotatingFileHandler(
            self.__create_log_file(),
            maxBytes=5 * 1024 * 1024,
            backupCount=2,
            delay=0)
        hndlr.setFormatter(logging.Formatter(LOG_FORMAT))
        hndlr.setLevel(logging.INFO)

        return hndlr

    def __create_log_file(self):
        """
        Метод для создания logger
        :return: log_file
        """
        if not os.path.exists(self.log_file):
            try:
                os.makedirs(self.log_file)
            except PermissionError:
                cprint(Messages.cant_create_folder.format(
                    self.log_file), color=Colors.red)
                self.log_file = os.sep.join(
                    [tempfile.gettempdir(), ProjectBIO.log_filename])
                cprint(Messages.temp_dir.format(
                    self.log_file), color=Colors.yellow)

        cprint(Messages.create_file.format(
            self.log_file), color=Colors.green)

        return self.log_file

    def __log_uncaught_exceptions(self, exception_type, exception_msg, traceback_msg):
        """
        Метод для логгирования необработанных исключений и завершения программы
        :param exception_type: Тип вызванного исключения
        :param exception_msg: Исключение (текст)
        :param traceback_msg: Ошибки построчно
        :return: -
        """
        self.logger.critical(
            Messages.miss_exception,
            EXCEPTION_FORMAT.format(
                error_type=exception_type.__name__,
                error_msg=exception_msg,
                traceback_msg="".join(traceback.format_tb(traceback_msg))))
        sys.exit()


class Writer(object):
    """
    Вспомогательный класс для взаимодействия с пользователем.
    Пишет сообщения в консоль и лог-файл.
    """
    def __new__(cls):
        """
        Обеспечивает работу класса в режиме синглтона.
        """
        if not hasattr(cls, 'instance'):
            cls.instance = super(Writer, cls).__new__(cls)
            cls.logger = Logger().logger
        return cls.instance

    @classmethod
    def my_print(cls, msg, msg_type='', out=False):
        """
        Метод, который принимат на вход сообщение,
            после чего пишет его в лог файл и выводит в консоль
        :param msg: Сообщение, которое будет записано в лог-файл
                    и выведено в консоль.
        :param msg_type: Тип сообщения, влияет на форматирование
                         выводимого в консоль сообщения
        """
        if msg_type == MessageTypes.info:
            cprint(msg, color=Colors.green)
            cls.logger.info('%s', msg)
        elif msg_type == MessageTypes.warning:
            cprint(msg, color=Colors.yellow)
            cls.logger.warning('%s', msg)
        elif msg_type == MessageTypes.critical:
            cprint(msg, color=Colors.red)
            cls.logger.critical('%s', msg)
        elif msg_type == MessageTypes.special:
            cprint(msg, color=Colors.blue)
            cls.logger.info('%s', msg)
        else:
            cprint(msg, color=Colors.white)
            cls.logger.info('%s', msg)
